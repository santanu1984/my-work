package Academy.E2EProject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	// in page object ,object should be defined at top and methods should be written at bottom.
	
	public WebDriver driver;
	By email=By.id("user_email");
	By Password=By.id("user_password");
	By Loginbutton=By.name("commit");
	
	public LoginPage(WebDriver driver) {
		
	this.driver=driver;
	}

	public WebElement getemail()
	{
		
		return driver.findElement(email);
		
	}

	public WebElement getpassword()
	{
		
	
		return driver.findElement(Password);
		
	}

	public WebElement getloginbutton()
	{
		
		return driver.findElement(Loginbutton);
		
		
	}		
		
	
	
}
	
	
	


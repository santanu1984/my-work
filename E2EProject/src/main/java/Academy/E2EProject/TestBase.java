package Academy.E2EProject;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class TestBase {
	
	public static WebDriver driver=null;
	
	//Example of global variable in Selenium
	
	public static WebDriver initializeDriver() throws IOException
	{
		Properties prop= new Properties();
		FileInputStream fis=new FileInputStream("C:\\Users\\tza1051\\eclipse-workspace_Selsetup_July12\\E2EProject\\src\\main\\java\\Academy\\E2EProject\\datadriven.properties");
	
	// connect prop and fis so that prop object can find the file
		
		prop.load(fis);

	// Choose Browser based on the feed in property file.			
		if(prop.getProperty("Browser").equals("Firefox"))
		{
			System.setProperty("webdriver.firefox.driver","C:\\Selenium setups\\selenium drivers and JAR\\geckodriver-v0.18.0-win64\\geckodriver.exe");
			driver= new FirefoxDriver();
		}
		else if(prop.getProperty("Browser").equals("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver","C:\\Selenium setups\\chromedriver_win32\\chromedriver.exe");
			driver= new ChromeDriver();
		}
		else		
		{
			System.setProperty("webdriver.ie.driver","C:\\Selenium setups\\selenium drivers and JAR\\IEDriverServer_x64_3.5.1\\IEDriverServer.exe");
			driver= new InternetExplorerDriver();
		}
		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		driver.get(prop.getProperty("Url"));
	
		return driver;
	
	}
	
	
}
